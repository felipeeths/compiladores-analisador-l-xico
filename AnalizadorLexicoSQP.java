/* Generated By:JavaCC: Do not edit this line. AnalizadorLexicoSQP.java */
class AnalizadorLexicoSQP implements AnalizadorLexicoSQPConstants {
        public static void main(String[] args) throws ParseException
        {
                try
                {
                        AnalizadorLexicoSQP analizador = new  AnalizadorLexicoSQP(System.in);
                        analizador.Start();

                }catch(ParseException e)
                {
                        System.out.println(e.getMessage());
                        System.out.println("Erro Encontrado na Analise");
                }
        }

/*****************************************
	Funcao para startar a o analisador lexico
 *****************************************/
  final public void Start() throws ParseException {
    label_1:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case OP_ARITIMETICA:
      case OP_LOGICO:
      case OP_RELACIONAL:
      case TIPO:
      case BEGIN:
      case END:
      case VOID:
      case MAIN:
      case ABRE_PAREN:
      case FECHA_PAREN:
      case IF:
      case WHILE:
      case FOR:
      case ELSE:
      case THEN:
      case RETURN:
      case ABRE_CH:
      case FECHA_CH:
      case PTVIRG:
      case IGUAL:
      case VIRGULA:
      case EXCLA:
      case NUM:
      case IDENT:
        ;
        break;
      default:
        jj_la1[0] = jj_gen;
        break label_1;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case BEGIN:
        jj_consume_token(BEGIN);
        break;
      case END:
        jj_consume_token(END);
        break;
      case VOID:
        jj_consume_token(VOID);
        break;
      case MAIN:
        jj_consume_token(MAIN);
        break;
      case ABRE_PAREN:
        jj_consume_token(ABRE_PAREN);
        break;
      case FECHA_PAREN:
        jj_consume_token(FECHA_PAREN);
        break;
      case IF:
        jj_consume_token(IF);
        break;
      case WHILE:
        jj_consume_token(WHILE);
        break;
      case FOR:
        jj_consume_token(FOR);
        break;
      case ELSE:
        jj_consume_token(ELSE);
        break;
      case THEN:
        jj_consume_token(THEN);
        break;
      case RETURN:
        jj_consume_token(RETURN);
        break;
      case ABRE_CH:
        jj_consume_token(ABRE_CH);
        break;
      case FECHA_CH:
        jj_consume_token(FECHA_CH);
        break;
      case PTVIRG:
        jj_consume_token(PTVIRG);
        break;
      case IGUAL:
        jj_consume_token(IGUAL);
        break;
      case IDENT:
        jj_consume_token(IDENT);
        break;
      case NUM:
        jj_consume_token(NUM);
        break;
      case TIPO:
        jj_consume_token(TIPO);
        break;
      case OP_RELACIONAL:
        jj_consume_token(OP_RELACIONAL);
        break;
      case OP_LOGICO:
        jj_consume_token(OP_LOGICO);
        break;
      case OP_ARITIMETICA:
        jj_consume_token(OP_ARITIMETICA);
        break;
      case VIRGULA:
        jj_consume_token(VIRGULA);
        break;
      case EXCLA:
        jj_consume_token(EXCLA);
        break;
      default:
        jj_la1[1] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
    jj_consume_token(0);
  }

  /** Generated Token Manager. */
  public AnalizadorLexicoSQPTokenManager token_source;
  JavaCharStream jj_input_stream;
  /** Current token. */
  public Token token;
  /** Next token. */
  public Token jj_nt;
  private int jj_ntk;
  private int jj_gen;
  final private int[] jj_la1 = new int[2];
  static private int[] jj_la1_0;
  static private int[] jj_la1_1;
  static {
      jj_la1_init_0();
      jj_la1_init_1();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0xffffe000,0xffffe000,};
   }
   private static void jj_la1_init_1() {
      jj_la1_1 = new int[] {0x1f,0x1f,};
   }

  /** Constructor with InputStream. */
  public AnalizadorLexicoSQP(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public AnalizadorLexicoSQP(java.io.InputStream stream, String encoding) {
    try { jj_input_stream = new JavaCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new AnalizadorLexicoSQPTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 2; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 2; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public AnalizadorLexicoSQP(java.io.Reader stream) {
    jj_input_stream = new JavaCharStream(stream, 1, 1);
    token_source = new AnalizadorLexicoSQPTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 2; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 2; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public AnalizadorLexicoSQP(AnalizadorLexicoSQPTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 2; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(AnalizadorLexicoSQPTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 2; i++) jj_la1[i] = -1;
  }

  private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  private int jj_ntk() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  private int[] jj_expentry;
  private int jj_kind = -1;

  /** Generate ParseException. */
  public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[38];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 2; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
          if ((jj_la1_1[i] & (1<<j)) != 0) {
            la1tokens[32+j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 38; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  final public void enable_tracing() {
  }

  /** Disable tracing. */
  final public void disable_tracing() {
  }

}
